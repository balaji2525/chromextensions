var seconds = 10;
const id = setInterval(() => {
  const btn = document.querySelector(
    "body > div.content > ui-view > ui-context-loader > div.ng-scope > div:nth-child(2) > at-work-space > ui-work-page-footer > footer > ng-transclude > div.pull-right.ng-scope > div:nth-child(3) > div > button.btn.btn-danger.gray"
  );
  if (btn) {
    btn.removeAttribute("disabled");
    console.log("removed Disabled Attribute");
    clearInterval(id);
  } else {
    console.log("Dom not loaded will retry in 1 sec");
  }
  if (seconds-- == 0) {
    clearInterval(id);
  }
}, 1000);
