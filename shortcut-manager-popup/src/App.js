import "./App.css"
import { useState } from "react"

function App() {
  const [shortcut, setShortcut] = useState([])

  const onAdd = () => {
    const line = (
      <div>
        <input type="text" placeholder="label" />
        <input type="text" placeholder="id" />
        <input type="text" placeholder="shortcut" />
      </div>
    )

    setShortcut((prev) => {
      if (prev.length === 0) {
        return line
      }
      prev.push(line)
      return prev
    })
  }

  return (
    <div className="App">
      {shortcut}
      <button onClick={onAdd}>add</button>
    </div>
  )
}

export default App
