var seconds = 10
const PLAY_BUTTON = "."
const FIRST_INVALID = "z"

const NOISE = "x"
const MUTE_PART_ISSUE = ","

const SECOND_INVALID = "c"
const SUBMIT = "v"

chrome.runtime.onMessage.addListener((obj, sender, response) => {
  addAppenChinaShortcuts()
})

function addSemiAutomations(playButton, invalidCheckbox, invalidCheckbox2, completeAndNextTaskBtn) {
  document.addEventListener("keydown", (event) => {
    // NOISE PART
    if (event.key === "1") {
      invalidCheckbox.click()
      invalidCheckbox2.click()
    }

    if (event.key === "4") {
      getValidCheckbox().click()
      getValidCheckbox2().click()
    }

    // MUTE PART
    if (event.key === "2") {
      getNoiseCheckbox().click()
    }

    if (event.key === "3") {
      getMutePartCheckbox().click()
    }

    if (event.key === "5") {
      playButton.click()
    }

    if (event.key === "Enter") {
      completeAndNextTaskBtn.click()
    }
  })
}

function addAppenChinaShortcuts() {
  const id = setInterval(() => {
    try {
      const playButton = getPlayButton()
      const invalidCheckbox = getInvalidCheckbox()

      const invalidCheckbox2 = getInvalidCheckbox2()
      const completeAndNextTaskBtn = getCompleteAndNextTaskBtn()

      if (playButton && invalidCheckbox && invalidCheckbox2 && completeAndNextTaskBtn) {
        invalidCheckbox.addEventListener("change", () => {
          const noiseCheckbox = getNoiseCheckbox()
          const mutePartCheckbox = getMutePartCheckbox()
  
          if (noiseCheckbox) {
            noiseCheckbox.setAttribute("accesskey", NOISE)
          }
          if (mutePartCheckbox) {
            mutePartCheckbox.setAttribute("accesskey", MUTE_PART_ISSUE)
          }
          console.log(`Noise/mutepart shortcut loaded`)
        })
  
        playButton.setAttribute("accesskey", PLAY_BUTTON)
        invalidCheckbox.setAttribute("accesskey", FIRST_INVALID)
        invalidCheckbox2.setAttribute("accesskey", SECOND_INVALID)
        completeAndNextTaskBtn.setAttribute("accesskey", SUBMIT)
        console.log(`Shortcuts loaded`)
        addSemiAutomations(playButton, invalidCheckbox, invalidCheckbox2, completeAndNextTaskBtn)
        clearInterval(id)
      } else {
        console.log("Dom not loaded will retry in 1 sec")
      }
    } catch (e) {
      console.log("Dom not YET")
    }
    if (seconds-- == 0) {
      clearInterval(id)
    }
  }, 1000)
}

function getElementByXpath(path) {
  return document.evaluate(path, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue
}

function getPlayButton() {
  return getElementByXpath("/html/body/div[2]/form/div[1]/div[1]/div/div/div/div/div[1]/div/div[2]/div[4]/button[1]")
}

function getInvalidCheckbox() {
  return getElementByXpath("/html/body/div[2]/form/div[1]/div[1]/div/div/div/div/div[1]/div/div[3]/label[2]/span[1]")
}

function getValidCheckbox() {
  return getElementByXpath("/html/body/div[2]/form/div[1]/div[1]/div/div/div/div/div[1]/div/div[3]/label[1]/span[1]")
}

function getValidCheckbox2() {
  return getElementByXpath("/html/body/div[2]/form/div[1]/div[1]/div/div/div/div/div[3]/div[2]/div/div/label[1]/span[1]/input")
}

function getNoiseCheckbox() {
  return getElementByXpath(
    "/html/body/div[2]/form/div[1]/div[1]/div/div/div/div/div[1]/div/div[4]/div/label[2]/span[1]/span"
  )
}
function getMutePartCheckbox() {
  return getElementByXpath(
    "/html/body/div[2]/form/div[1]/div[1]/div/div/div/div/div[1]/div/div[4]/div/label[12]/span[1]/span"
  )
}
function getInvalidCheckbox2() {
  return getElementByXpath(
    "/html/body/div[2]/form/div[1]/div[1]/div/div/div/div/div[3]/div[2]/div/div/label[2]/span[1]/input"
  )
}
function getCompleteAndNextTaskBtn() {
  return getElementByXpath("/html/body/div[2]/form/div[1]/div[2]/div/div/div[3]/button[2]/span")
}
