var seconds = 10;
const id = setInterval(() => {
  const btn = document.querySelector("[id^='wavebtn_audio_']");
  if (btn) {
    btn.setAttribute("accesskey", ".");
    console.log("Added shortcut");
    clearInterval(id);
  } else {
    console.log("Dom not loaded will retry in 1 sec");
  }
  if (seconds-- == 0) {
    clearInterval(id);
  }
}, 1000);
